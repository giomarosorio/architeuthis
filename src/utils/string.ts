export function isString(value: any): value is string {
  return typeof value === 'string';
}

export function isNonEmptyString(value: any): value is string {
  return isString(value) && value !== '';
}
