import { storageAdapter } from '../../adapters';
import { Http } from '../../core';
import { StorageElement } from '../../models/';
import { DAO } from '../models';

export class StorageElementDAO implements DAO<StorageElement> {
  private client: Http;
  constructor(client: Http) {
    this.client = client;
  }
  private ErrorOrT = <T>(result: any): Error | T => {
    return typeof result === typeof Error ? (result as Error) : (result as T);
  };
  getById = async (path: string, bucketName: string): Promise<StorageElement | Error> => {
    const result = await this.client.get<StorageElement>({ bucketName: bucketName, path: path }, storageAdapter);
    return this.ErrorOrT<StorageElement>(result);
  };
  createById = async (path: string, body: StorageElement, bucketName: string): Promise<StorageElement | Error> => {
    const result = await this.client.post<StorageElement>({ bucketName: bucketName, path: path, body: body }, storageAdapter);
    return this.ErrorOrT<StorageElement>(result);
  };
  updateById = async (path: string, body: StorageElement, bucketName: string): Promise<StorageElement | Error> => {
    const result = await this.client.put<StorageElement>({ bucketName: bucketName, path: path, body: body }, storageAdapter);
    return this.ErrorOrT<StorageElement>(result);
  };
  deleteById = async (path: string, bucketName: string): Promise<void | Error> => {
    const result = await this.client.delete({ bucketName: bucketName, path: path });
    return this.ErrorOrT<null>(result);
  };
}
