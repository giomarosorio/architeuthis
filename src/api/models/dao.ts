export interface DAO<M> {
  getById: (path: string, bucketName?: string) => Promise<M | Error>;
  createById: (path: string, body: any, bucketName?: string) => Promise<M | Error>;
  updateById: (path: string, body: any, bucketName?: string) => Promise<M | Error>;
  deleteById: (path: string, bucketName?: string) => Promise<void | Error>;
}
