import { DeletePayload, GetPayload, PostPayload, PutPayload } from '../../../core/models';
import { CRITICAL, errorBuilder, WARNING } from '../../../utils/error';
import { FirebaseAdmin } from '../..';

const checkBucket = async (client: FirebaseAdmin.config.StorageI, bucketName: string): Promise<boolean> => {
  if (typeof bucketName === 'undefined') return false;
  return (await client.bucket(bucketName).exists())[0];
};

const checkFile = async (client: FirebaseAdmin.config.StorageI, bucketName: string, path: string): Promise<boolean> => {
  if (typeof bucketName === 'undefined' || typeof path === 'undefined') return false;
  return (await client.bucket(bucketName).file(path).exists())[0];
};

export const get =
  <T>(client: FirebaseAdmin.config.StorageI) =>
  async ({ bucketName, path }: GetPayload): Promise<T | Error> => {
    try {
      if (!(await checkBucket(client, bucketName))) return errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });
      if (!(await checkFile(client, bucketName, path))) return errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });
      const fileMetadata = await client.bucket(bucketName).file(path).getMetadata();
      const fileContent = (await client.bucket(bucketName).file(path).download())[0];
      return { metadata: fileMetadata, content: fileContent } as unknown as T;
    } catch (error) {
      throw errorBuilder({ type: CRITICAL, code: 500, message: `STORAGE<GET> ${error.message}` });
    }
  };
export const post =
  <T>(client: FirebaseAdmin.config.StorageI) =>
  async ({ bucketName, path, body }: PostPayload): Promise<T | Error> => {
    try {
      if (!(await checkBucket(client, bucketName))) return errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });
      if (await checkFile(client, bucketName, path))
        return errorBuilder({
          type: WARNING,
          code: 405,
          message: 'Action not allowed. It is only possible to execute GET, PUT and DELETE on the file.',
        });
      await client.bucket(bucketName).file(path).save(body);
      const fileMetadata = await client.bucket(bucketName).file(path).getMetadata();
      const fileContent = (await client.bucket(bucketName).file(path).download())[0];
      return { metadata: fileMetadata, content: fileContent } as unknown as T;
    } catch (error) {
      throw errorBuilder({ type: CRITICAL, code: 500, message: `STORAGE<POST> ${error.message}` });
    }
  };

export const put =
  <T>(client: FirebaseAdmin.config.StorageI) =>
  async ({ bucketName, path, body }: PutPayload): Promise<T | Error> => {
    try {
      if (!(await checkBucket(client, bucketName))) return errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });
      if (!(await checkFile(client, bucketName, path))) return errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });
      await client.bucket(bucketName).file(path).save(body);
      const fileMetadata = await client.bucket(bucketName).file(path).getMetadata();
      const fileContent = (await client.bucket(bucketName).file(path).download())[0];
      return { metadata: fileMetadata, content: fileContent } as unknown as T;
    } catch (error) {
      throw errorBuilder({ type: CRITICAL, code: 500, message: `STORAGE<PUT> ${error.message}` });
    }
  };

export const remove =
  <T>(client: FirebaseAdmin.config.StorageI) =>
  async ({ bucketName, path }: DeletePayload): Promise<T | Error> => {
    try {
      if (!(await checkBucket(client, bucketName))) return errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });
      if (!(await checkFile(client, bucketName, path))) return errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });
      await client.bucket(bucketName).file(path).delete();
    } catch (error) {
      throw errorBuilder({ type: CRITICAL, code: 500, message: `STORAGE<DELETE> ${error.message}` });
    }
  };
