# Architeuthis

![Architeuthis](/repo_img/architeuthis.png)

## About

Architeuthis makes development easy by offering an abstraction layer to
third-party services.

## Before use

### NPM dependencies

Clone this repository, open your console and move to the repository directory,
once there run one of the following commands

- npm i
- npm install

#### Depencendies in Developing

Make sure you have the .env file in the root of the project for the correct
docker build.

#### Dependencies in Production

Make sure you have the necessary environment variables for the correct docker build.

## How to use

Once configured, open your terminal go to the project folder and run the
following command

### How to use in Developing

- npm run dev (environment with automatic reload when saving files)

### How to use in Production

- npm run pro:docker-build:up (build and up docker image)

## Environments

### Production Environment

```
not available yet
```

### Staging/Development Environment

```
not available yet
```

## Deployment

### Deployment in Production

If you want to deploy kraken in environment prod, you must consider the followin
g variables.

- `GOOGLE_APPLICATION_CREDENTIALS`: GCP crendentials.

- `REGION`: The region where the machine works.

### Deployment in Staging/Development

If you want to deploy kraken in environment dev, you must consider the following
variables.

- `HEROKU_TOKEN_DEV`: Heroku credentials.

- `DATABASE_FIREBASE_DEV`: URL of the Firebase database.

- `CREDENTIALS_DEV`: Firebase crendentials.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md)
file for details

