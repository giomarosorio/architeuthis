#!/bin/bash
#
#===================================
set -e
#===================================
#===================================
#===================================
__dir_script="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__dir_root=${__dir_script%/*}
commands_to_find=('curl' 'jq' 'docker-compose')
env_path=""
storageurl=%1.appspot.com
databaseurl=https://%1.firebaseio.com
ENV="dev"
#===================================
#==============UTILS================
#===================================
source ${__dir_script}/utils.sh
source ${__dir_script}/docker_install.sh
#===================================
#==========DEPENDENCIES=============
#===================================
dependencies_env() {
    showmessage "  -Checking env dependencies..."
   [[ -e "$__dir_root/.env" ]] \
       && { env_path="$__dir_root/.env"; } \
       || { [[ -e "$__dir_root/.env.dev" ]] \
            && { env_path="$__dir_root/.env.dev"; } \
            || { showmessage "Error. It is necessary to provide an .env or .env.dev file for the execution of 'npm run dev'."; 
                 exit 126; } 
          }
   [[ -n $(cat "$env_path") ]] \
       && { SERVICEACCOUNT=$(cat "$env_path");
            PROJECT_ID=$(cat "$env_path" | jq -r '.project_id');
            STORAGE="${storageurl//%1/$PROJECT_ID}";
            DATABASE="${databaseurl//%1/$PROJECT_ID}"; } \
       || { showmessage "the '$env_path' file is empty, provide the details of a valid service account.";
            exit 126; }
}
#===================================
install_dependencies() {
    [[ -n $(service docker >/dev/null 2>&1 | grep "unrecognized") ]] \
        && { showmessage "Installing Docker..."; docker_install; showmessage "Docker installed."; } \
        || { return 0; }
    check_commands "${commands_to_find[@]}"
    [[ -n "$packages_to_install" ]] \
        && { showmessage "Installing dependencies....";
             showcommand "sudo apt-get install${packages_to_install} -y";
             sudo apt-get install$packages_to_install -y;
             showmessage "\ndependencies installed successfully"; } \
        || { return 0; }
}
#===================================
dependencies_packages() {
    showmessage "  -Checking packages dependencies..."
    install_dependencies
}
#===================================
dependencies() {
    showmessage "-Checking dependencies..."
    dependencies_env
    dependencies_packages
    showmessage "+Dependencies fulfilled."
}
#===================================
#=============Docker================
#===================================
check_service(){
    showmessage "  -Checking service status..."
    [[ -n $(sudo service docker status | grep "is not") ]] && { sudo service docker start; }
    showmessage "  +Docker service is started."
}
#===================================
docker_build_up() {
    showmessage "  -Running Docker container..."
    docker-compose -f docker-compose.dev:nodemon-builder.yaml run --rm install
    NODE_ENV="${ENV}" DATABASE="${DATABASE}" STORAGE="${STORAGE}" GOOGLE_APPLICATION_CREDENTIALS="${SERVICEACCOUNT}" docker-compose -f docker-compose.dev:nodemon.yaml up
    showmessage "  +The docker container has been stopped."
}
#===================================
docker(){
    showmessage "-Checking Docker."
    check_service
    docker_build_up
}
#===================================
#===========Run Script==============
#===================================
run() {
    showtitle "Starting Architeuthis DEV"
    dependencies
    docker
}
#===================================
run
exit 0
