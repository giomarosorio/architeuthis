FROM node:15.8.0-alpine3.10

# Install dependencies
RUN apk update && apk upgrade

# Set the timezone in docker
RUN apk --update add tzdata && cp /usr/share/zoneinfo/America/Bogota /etc/localtime && echo "America/Bogota" > /etc/timezone && apk del tzdata

# Switch Work Directory
WORKDIR /usr/src/architeuthis

# Copy the package.json and package-lock.json files to work directory
COPY package*.json ./

# Install all Packages
RUN npm install

# Copy all other source code to work directory
COPY . .

# Expose 
EXPOSE 3000

# Start
CMD ["npm", "start"]
