/* eslint-disable max-lines */
import { ApiError, ServiceObjectConfig } from '@google-cloud/common';
import { Bucket, File } from '@google-cloud/storage';
import * as fs from 'fs';

import { StorageElement } from '../../../core/models/storage';
import { StorageClientMock } from '../firebase-admin/storage';

export type PublicInterfaceOf<Class> = {
  [Member in keyof Class]: Class[Member];
};
export type FilesInBucket<T> = { [index: string]: T };
export type MakeAllFilesPublicPrivateResponse = [File[]];
export interface BucketOptions {
  userProject?: string;
}

export const fileDirectory = `${__dirname}/files/`;

const methods = {
  create: { reqOpts: { qs: '' } },
  delete: { reqOpts: { qs: '' } },
  exists: { reqOpts: { qs: '' } },
  get: { reqOpts: { qs: '' } },
  getMetadata: { reqOpts: { qs: '' } },
  setMetadata: { reqOpts: { qs: '' } },
};

const TEST_AUTO_RETRY_DEFAULT = true;
const TEST_MAX_RETRY_DEFAULT = 3;
const TEST_RETRY_DELAY_MULTIPLIER_DEFAULT = 2;
const TEST_TOTAL_TIMEOUT_DEFAULT = 600;
const TEST_MAX_RETRY_DELAY_DEFAULT = 64;
const TEST_BASE_URL = 'https://storage.googleapis.com/storage/v1';
const TEST_CUSTOM_ENDPOINT = false;
const TEST_PROJECT_ID_REQUIRED = false;
const TEST_SCOPES = [
  'https://www.googleapis.com/auth/iam',
  'https://www.googleapis.com/auth/cloud-platform',
  'https://www.googleapis.com/auth/devstorage.full_control',
];
export const TEST_RETRYABLE_ERR_FN_DEFAULT = function (err?: ApiError) {
  if (err) {
    if ([408, 429, 500, 502, 503, 504].indexOf(err.code!) !== -1) {
      return true;
    }

    if (err.errors) {
      for (const e of err.errors) {
        const reason = e.reason?.toLowerCase();
        if (
          (reason && reason.includes('eai_again')) || //DNS lookup error
          reason === 'connection reset by peer' ||
          reason === 'unexpected connection closure'
        ) {
          return true;
        }
      }
    }
  }
  return false;
};

const fixedEncodeURIComponent = (str: string): string => {
  return encodeURIComponent(str).replace(/[!'()*]/g, c => '%' + c.charCodeAt(0).toString(16).toUpperCase());
};

const encodeURI = (uri: string, encodeSlash: boolean): string => {
  // Split the string by `/`, and conditionally rejoin them with either
  // %2F if encodeSlash is `true`, or '/' if `false`.
  return uri
    .split('/')
    .map(fixedEncodeURIComponent)
    .join(encodeSlash ? '%2F' : '/');
};

export const TEST_CONFIG_SERVICE = {
  apiEndpoint: 'https://storage.googleapis.com',
  retryOptions: {
    autoRetry: TEST_AUTO_RETRY_DEFAULT,
    maxRetries: TEST_MAX_RETRY_DEFAULT,
    retryDelayMultiplier: TEST_RETRY_DELAY_MULTIPLIER_DEFAULT,
    totalTimeout: TEST_TOTAL_TIMEOUT_DEFAULT,
    maxRetryDelay: TEST_MAX_RETRY_DELAY_DEFAULT,
    retryableErrorFn: TEST_RETRYABLE_ERR_FN_DEFAULT,
  },
  baseUrl: TEST_BASE_URL,
  customEndpoint: TEST_CUSTOM_ENDPOINT,
  projectIdRequired: TEST_PROJECT_ID_REQUIRED,
  scopes: TEST_SCOPES,
  packageJson: require('./package-test.json'),
};

export const TEST_CONFIG_SERVICEOBJECT = (storage: StorageClientMock, name: string): ServiceObjectConfig => {
  return {
    parent: storage,
    baseUrl: '/b',
    id: name,
    createMethod: storage.createBucket.bind(storage),
    methods,
  };
};

export const TEST_CONFIG_SERVICEOBJECT_FILE = (storage: Bucket, name: string): ServiceObjectConfig => {
  return {
    parent: storage,
    baseUrl: '/o',
    id: encodeURI(name, true),
    methods,
  };
};

export const makeFilesInBucket = (fileList: string[]): FilesInBucket<StorageElement> => {
  let files: FilesInBucket<StorageElement> = {};
  let date: Date = new Date();
  fileList.forEach(value => {
    const fileName = value;
    const fileSize = `${Math.floor(Math.random() * 1000000000) + 1} bytes`;
    const fileType = value.split('.')[1];
    const fileCreated = date.toString();
    const fileUpdated = date.toString();
    const fileContent = fs.readFileSync(`${__dirname}/files/${value}`);
    files[value] = {
      metadata: {
        name: fileName,
        size: fileSize,
        type: fileType,
        created: fileCreated,
        updated: fileUpdated,
      },
      content: fileContent,
    };
  });
  return files;
};
