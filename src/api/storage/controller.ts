import { StorageElementDAO } from '../daos';
import { callbackBuilder, Injection } from '../models';

export const getElement = ({ storage: { gcp } }: Injection) =>
  callbackBuilder(async (req, res, next) => {
    try {
      const filePath = req.params.path;
      const bucketName = req.params.bucket;
      const dao = new StorageElementDAO(gcp);
      const result = await dao.getById(filePath, bucketName);
      return res.send(result);
    } catch (error) {
      return next(error);
    }
  });

export const deleteElement = ({ storage: { gcp } }: Injection) =>
  callbackBuilder((req, res, next) => {
    return res.send(`Element ${req.method}, id: ${req.params.id}`);
  });

export const postElement = ({ storage: { gcp } }: Injection) =>
  callbackBuilder((req, res, next) => {
    return res.send(`Element ${req.method}, id: ${req.params.id}`);
  });

export const putElement = ({ storage: { gcp } }: Injection) =>
  callbackBuilder((req, res, next) => {
    return res.send(`Element ${req.method}, id: ${req.params.id}`);
  });
