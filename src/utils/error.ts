export const CRITICAL = 'CRITICAL';
export const WARNING = 'WARNING';

export type ErrorTypes = typeof CRITICAL | typeof WARNING;

export interface ErrorHandler {
  type: ErrorTypes;
  message: string;
  code: number;
}

export const errorBuilder = ({ type, message, code }: ErrorHandler): Error => {
  const encode = JSON.stringify({ type, message, code });
  return new Error(encode);
};

export const errorReceiver = (error: string): ErrorHandler => JSON.parse(error) as ErrorHandler;
