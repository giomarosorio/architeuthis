/* eslint-disable max-lines */
import { BodyResponseCallback, DecorateRequestOptions, Service, ServiceObject } from '@google-cloud/common';
import {
  DeleteCallback,
  DeleteOptions,
  ExistsCallback,
  ExistsOptions,
  Metadata,
  MetadataResponse,
} from '@google-cloud/common/build/src/service-object';
import { Duplexify } from '@google-cloud/common/build/src/util';
import {
  Bucket,
  BucketCallback,
  Channel,
  CopyCallback,
  CopyOptions,
  CopyResponse,
  CreateBucketRequest,
  CreateBucketResponse,
  CreateChannelConfig,
  CreateChannelResponse,
  CreateHmacKeyCallback,
  CreateHmacKeyOptions,
  CreateHmacKeyResponse,
  CreateNotificationResponse,
  CreateReadStreamOptions,
  CreateResumableUploadCallback,
  CreateResumableUploadOptions,
  CreateResumableUploadResponse,
  CreateWriteStreamOptions,
  DeleteLabelsResponse,
  DisableRequesterPaysResponse,
  DownloadCallback,
  DownloadOptions,
  DownloadResponse,
  EnableRequesterPaysResponse,
  File,
  FileOptions,
  GenerateSignedPostPolicyV2Callback,
  GenerateSignedPostPolicyV2Options,
  GenerateSignedPostPolicyV2Response,
  GenerateSignedPostPolicyV4Callback,
  GenerateSignedPostPolicyV4Options,
  GenerateSignedPostPolicyV4Response,
  GetBucketsCallback,
  GetBucketsRequest,
  GetBucketsResponse,
  GetExpirationDateCallback,
  GetExpirationDateResponse,
  GetHmacKeysCallback,
  GetHmacKeysOptions,
  GetHmacKeysResponse,
  GetServiceAccountCallback,
  GetServiceAccountOptions,
  GetServiceAccountResponse,
  GetSignedPolicyCallback,
  GetSignedPolicyOptions,
  GetSignedPolicyResponse,
  GetSignedUrlCallback,
  GetSignedUrlConfig,
  HmacKey,
  Iam,
  MakeFilePrivateCallback,
  MakeFilePrivateOptions,
  MakeFilePrivateResponse,
  MakeFilePublicResponse,
  MoveCallback,
  MoveOptions,
  MoveResponse,
  RotateEncryptionKeyCallback,
  RotateEncryptionKeyOptions,
  RotateEncryptionKeyResponse,
  SaveCallback,
  SaveOptions,
  SetStorageClassCallback,
  SetStorageClassOptions,
  SetStorageClassResponse,
  Storage,
  StorageOptions,
} from '@google-cloud/storage';
import { Acl } from '@google-cloud/storage/build/src/acl';
import {
  BucketLockResponse,
  CombineResponse,
  EnableLoggingOptions,
  GetBucketSignedUrlConfig,
  GetFilesResponse,
  GetLabelsResponse,
  GetNotificationsResponse,
  Labels,
  LifecycleRule,
  MakeBucketPrivateResponse,
  MakeBucketPublicResponse,
  SetBucketMetadataResponse,
  SetLabelsResponse,
  UploadResponse,
} from '@google-cloud/storage/build/src/bucket';
import { IsPublicResponse, RenameCallback, RenameOptions, RenameResponse } from '@google-cloud/storage/build/src/file';
import { HmacKeyOptions } from '@google-cloud/storage/build/src/hmacKey';
import { GetSignedUrlResponse, URLSigner } from '@google-cloud/storage/build/src/signer';
import { Cors } from '@google-cloud/storage/build/src/storage';
import { Readable, Writable } from 'stream';
import * as r from 'teeny-request';

import { StorageElement } from '../../../core/models/storage';
import { isNonEmptyString } from '../../../utils/string';
import { AppI, StorageI } from '../../firebase-admin/config';
import {
  BucketOptions,
  FilesInBucket,
  MakeAllFilesPublicPrivateResponse,
  PublicInterfaceOf,
  TEST_CONFIG_SERVICE,
  TEST_CONFIG_SERVICEOBJECT,
  TEST_CONFIG_SERVICEOBJECT_FILE,
} from '../utils';

class FileMock extends ServiceObject<File> implements PublicInterfaceOf<File> {
  acl: Acl;
  bucket: BucketMock;
  storage: Storage;
  kmsKeyName?: string;
  userProject?: string;
  signer?: URLSigner;
  metadata: Metadata;
  name: string;
  generation?: number;
  parent!: BucketMock;
  constructor(bucket: BucketMock, name: string, options?: FileOptions) {
    super(TEST_CONFIG_SERVICEOBJECT_FILE(bucket, name));
    this.bucket = bucket;
    this.parent = bucket;
    this.storage = bucket.storage;
    this.name = name;
  }
  copy(destination: string | Bucket | File, options: CopyOptions | CopyCallback): Promise<CopyResponse> {
    throw new Error('Method not implemented.');
  }
  createWriteStream(options?: CreateWriteStreamOptions): Writable {
    throw new Error('Method not implemented.');
  }
  createReadStream(options?: CreateReadStreamOptions): Readable {
    throw new Error('Method not implemented.');
  }
  createResumableUpload(options: CreateResumableUploadOptions | CreateResumableUploadCallback): Promise<CreateResumableUploadResponse> {
    throw new Error('Method not implemented.');
  }
  deleteResumableCache(): void {
    throw new Error('Method not implemented.');
  }
  download(options: DownloadOptions | DownloadCallback): Promise<DownloadResponse> {
    return Promise.resolve([this.bucket.fileData(this.name).content]);
  }
  setEncryptionKey(encryptionKey: string | Buffer): File {
    throw new Error('Method not implemented.');
  }
  getExpirationDate(callback?: GetExpirationDateCallback): Promise<GetExpirationDateResponse> {
    throw new Error('Method not implemented.');
  }
  getSignedPolicy(options: GetSignedPolicyOptions | GetSignedPolicyCallback): Promise<GetSignedPolicyResponse> {
    throw new Error('Method not implemented.');
  }
  generateSignedPostPolicyV2(
    options: GenerateSignedPostPolicyV2Options | GenerateSignedPostPolicyV2Callback,
  ): Promise<GenerateSignedPostPolicyV2Response> {
    throw new Error('Method not implemented.');
  }
  generateSignedPostPolicyV4(
    options: GenerateSignedPostPolicyV4Options | GenerateSignedPostPolicyV4Callback,
  ): Promise<GenerateSignedPostPolicyV4Response> {
    throw new Error('Method not implemented.');
  }
  getSignedUrl(cfg: GetSignedUrlConfig | GetSignedUrlCallback): Promise<GetSignedUrlResponse> {
    throw new Error('Method not implemented.');
  }
  isPublic(): Promise<IsPublicResponse> {
    throw new Error('Method not implemented.');
  }
  makePrivate(options: MakeFilePrivateOptions | MakeFilePrivateCallback): Promise<MakeFilePrivateResponse> {
    throw new Error('Method not implemented.');
  }
  makePublic(): Promise<MakeFilePublicResponse> {
    throw new Error('Method not implemented.');
  }
  publicUrl(): string {
    throw new Error('Method not implemented.');
  }
  move(destination: string | Bucket | File, options: MoveOptions | MoveCallback): Promise<MoveResponse> {
    throw new Error('Method not implemented.');
  }
  rename(destinationFile: string | File, options: RenameOptions | RenameCallback): Promise<RenameResponse> {
    throw new Error('Method not implemented.');
  }
  rotateEncryptionKey(options: RotateEncryptionKeyOptions | RotateEncryptionKeyCallback): Promise<RotateEncryptionKeyResponse> {
    throw new Error('Method not implemented.');
  }
  save(data: string | Buffer | StorageElement, options: SaveOptions | SaveCallback): Promise<void> {
    return new Promise(resolve => {
      this.bucket.fileSave(this.name, data as StorageElement);
      setTimeout(resolve, 100);
    });
  }
  setStorageClass(storageClass: string, options: SetStorageClassOptions | SetStorageClassCallback): Promise<SetStorageClassResponse> {
    throw new Error('Method not implemented.');
  }
  setUserProject(userProject: string): void {
    throw new Error('Method not implemented.');
  }
  startResumableUpload_(dup: Duplexify, options: CreateResumableUploadOptions): void {
    throw new Error('Method not implemented.');
  }
  startSimpleUpload_(dup: Duplexify, options?: CreateWriteStreamOptions): void {
    throw new Error('Method not implemented.');
  }
  async exists(options?: ExistsOptions | ExistsCallback): Promise<[boolean]> {
    return [this.bucket.fileExist(this.name)];
  }
  getMetadata(options?: object): Promise<MetadataResponse | Metadata> {
    return Promise.resolve(this.bucket.fileData(this.name).metadata);
  }
  delete(options?: DeleteOptions): Promise<[r.Response]>;
  delete(options: DeleteOptions, callback: DeleteCallback): void;
  delete(callback: DeleteCallback): void;
  delete(optionsOrCallback?: DeleteOptions | DeleteCallback, cb?: DeleteCallback): Promise<[r.Response]> | void {
    this.bucket.fileDelete(this.name);
  }
}

export class BucketMock extends ServiceObject implements PublicInterfaceOf<Bucket> {
  metadata: Metadata;
  name: string;
  storage: StorageClientMock;
  userProject?: string;
  acl: Acl;
  iam: Iam;
  getFilesStream: Function;
  signer?: URLSigner;
  private files: FilesInBucket<StorageElement>;
  constructor(storage: StorageClientMock, name: string, files?: FilesInBucket<StorageElement>, options?: BucketOptions) {
    super(TEST_CONFIG_SERVICEOBJECT(storage, name));
    this.storage = storage;
    this.name = name;
    this.files = typeof files !== 'undefined' ? files : {};
  }
  addLifecycleRule(rule: LifecycleRule): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented.');
  }
  combine(sources: string[] | File[], destination: string | File): Promise<CombineResponse> {
    throw new Error('Method not implemented');
  }
  createChannel(id: string, config: CreateChannelConfig): Promise<CreateChannelResponse> {
    throw new Error('Method not implemented');
  }
  createNotification(topic: string): Promise<CreateNotificationResponse> {
    throw new Error('Method not implemented');
  }
  deleteFiles(): Promise<void> {
    throw new Error('Method not implemented');
  }
  deleteLabels(): Promise<DeleteLabelsResponse> {
    throw new Error('Method not implemented');
  }
  disableRequesterPays(): Promise<DisableRequesterPaysResponse> {
    throw new Error('Method not implemented');
  }
  enableLogging(config: EnableLoggingOptions): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented');
  }
  enableRequesterPays(): Promise<EnableRequesterPaysResponse> {
    throw new Error('Method not implemented');
  }
  file(name: string, options?: FileOptions): File {
    return new FileMock(this, name, options);
  }
  fileExist(name: string): boolean {
    return this.files[name] !== undefined ? true : false;
  }
  fileData(name: string): StorageElement {
    return this.files[name];
  }
  fileSave(name: string, body: StorageElement): void {
    this.files[name] = body;
  }
  fileDelete(name: string): void {
    delete this.files[name];
  }
  getFiles(): Promise<GetFilesResponse> {
    throw new Error('Method not implemented');
  }
  getLabels(): Promise<GetLabelsResponse> {
    throw new Error('Method not implemented');
  }
  getNotifications(): Promise<GetNotificationsResponse> {
    throw new Error('Method not implemented');
  }
  getSignedUrl(cfg: GetBucketSignedUrlConfig): Promise<GetSignedUrlResponse> {
    throw new Error('Method not implemented');
  }
  lock(metageneration: string | number): Promise<BucketLockResponse> {
    throw new Error('Method not implemented');
  }
  makePrivate(): Promise<MakeBucketPrivateResponse> {
    throw new Error('Method not implemented');
  }
  makePublic(): Promise<MakeBucketPublicResponse> {
    throw new Error('Method not implemented');
  }
  notification(id: string): any {
    throw new Error('Method not implemented');
  }
  removeRetentionPeriod(): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented');
  }
  request(reqOpts: DecorateRequestOptions): Promise<[any, any]> {
    throw new Error('Method not implemented');
  }
  setLabels(labels: Labels): Promise<SetLabelsResponse> {
    throw new Error('Method not implemented');
  }
  setRetentionPeriod(duration: number): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented');
  }
  setCorsConfiguration(corsConfiguration: Cors[]): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented');
  }
  setStorageClass(storageClass: string): Promise<SetBucketMetadataResponse> {
    throw new Error('Method not implemented');
  }
  setUserProject(userProject: string): void {
    throw new Error('Method not implemented');
  }
  upload(pathString: string): Promise<UploadResponse> {
    throw new Error('Method not implemented');
  }
  makeAllFilesPublicPrivate_(): Promise<MakeAllFilesPublicPrivateResponse> {
    throw new Error('Method not implemented');
  }
  getId(): string {
    throw new Error('Method not implemented');
  }
  async exists(options?: object): Promise<[boolean]> {
    return [this.storage.bucketExist(this.name)];
  }
}

export class StorageClientMock extends Service implements PublicInterfaceOf<Storage> {
  getBucketsStream: () => Readable;
  getHmacKeysStream: () => Readable;
  acl: {
    OWNER_ROLE: string;
    READER_ROLE: string;
    WRITER_ROLE: string;
  };
  private buckets: { [index: string]: Bucket };
  constructor(files: FilesInBucket<StorageElement>, options?: StorageOptions) {
    super(TEST_CONFIG_SERVICE, options);
    this.buckets = {
      defaultBucket: new BucketMock(this, 'defaultBucket', files),
    };
  }
  bucket(name: string, options?: BucketOptions, files?: FilesInBucket<StorageElement>): Bucket {
    return this.buckets[name] !== undefined ? this.buckets[name] : new BucketMock(this, name, files);
  }
  bucketExist(name: string): boolean {
    return this.buckets[name] !== undefined ? true : false;
  }
  channel(id: string, resourceId: string): Channel {
    throw new Error('Method not implemented.');
  }
  async createBucket(name: string, metadata?: CreateBucketRequest | BucketCallback): Promise<CreateBucketResponse> {
    if (this.buckets[name] === undefined) {
      this.buckets[name] = new BucketMock(this, name);
      return [this.buckets[name], metadata];
    }
    throw new Error('Bucket already exists.');
  }
  createHmacKey(serviceAccountEmail: string, options?: CreateHmacKeyOptions | CreateHmacKeyCallback): Promise<CreateHmacKeyResponse> {
    throw new Error('Method not implemented.');
  }
  getBuckets(options?: GetBucketsRequest | GetBucketsCallback): Promise<GetBucketsResponse> {
    throw new Error('Method not implemented.');
  }
  getHmacKeys(options?: GetHmacKeysOptions | GetHmacKeysCallback): Promise<GetHmacKeysResponse> {
    throw new Error('Method not implemented.');
  }
  getServiceAccount(options?: GetServiceAccountOptions | GetServiceAccountCallback): Promise<GetServiceAccountResponse> {
    throw new Error('Method not implemented.');
  }
  hmacKey(accessId: string, options?: HmacKeyOptions): HmacKey {
    throw new Error('Method not implemented.');
  }
  getRequestInterceptors(): Function[] {
    throw new Error('Method not implemented.');
  }
  getProjectId(): Promise<string> {
    throw new Error('Method not implemented.');
  }
  request(reqOpts: DecorateRequestOptions, callback: BodyResponseCallback): void {
    throw new Error('Method not implemented.');
  }
  requestStream(reqOpts: DecorateRequestOptions): r.Request {
    throw new Error('Method not implemented.');
  }
}

export class StorageMock implements StorageI {
  private readonly storageClient: StorageClientMock;
  constructor(files?: FilesInBucket<StorageElement>) {
    this.storageClient = new StorageClientMock(files);
  }
  app: AppI;
  bucket(name?: string): Bucket {
    const bucketName = typeof name !== 'undefined' ? (isNonEmptyString(name) ? name : 'defaultBucket') : 'defaultBucket';
    return this.storageClient.bucket(bucketName);
  }
}
