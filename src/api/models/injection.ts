import { Http, Storage } from '../../core';

interface DatabaseClients {
  firestore: any;
  realtime: any;
}

interface AuthClients {
  gcp: any;
}

interface StorageClients {
  gcp: Http;
}

export interface Injection {
  database: DatabaseClients;
  auth: AuthClients;
  storage: StorageClients;
}

export const Dependencies: Injection = {
  storage: { gcp: Storage.Client },
  auth: { gcp: null },
  database: { firestore: null, realtime: null },
};
