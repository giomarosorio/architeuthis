#!/bin/bash
#
#===================================
set -e
#===================================
#======Set up the repository========
#===================================
set_up_the_repository(){
    sudo apt-get update -y 
    check_success_command "\nOops! something went wrong when trying to update." 
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release -y
    check_success_command "\nOops! something went wrong installing docker dependencies."
}
#===================================
#======Docker’s official GPG========
#===================================
dockers_official_GPC(){
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    check_success_command "\nOops! something went wrong installing docker GPC."
}
#===================================
#======Set stable repository========
#===================================
dockers_set_repository_branch() {
    echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    check_success_command "\nOops! something went wrong setting docker stable branch."
}
#===================================
#==========Docker Engine============
#===================================
docker_engine(){
    sudo apt-get update -y
    check_success_command "\nOops! something went wrong when trying to update."
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
    check_success_command "\nOops! something went wrong installing docker."
}
#===================================
#===========Post Install============
#===================================
post_install(){
    [[ -n $(uname -a | grep "microsoft") ]] \
        && { sudo service docker start; } \
        || { systemctl enable docker -f; sudo service docker start; }
    [[ -z $(cat /etc/group | grep "docker") ]] \
        && { sudo groupadd docker; } \
        || { return 0; }
    check_success_command "\nOops! something went wrong adding the docker group."
    sudo usermod -aG docker $USER
    check_success_command "\nOops! something went wrong adding the user to the docker group."
    newgrp docker
    check_success_command "\nOops! something went wrong when activating the changes in the docker group."
}
#===================================
#===================================
#===================================
docker_install(){
    set_up_the_repository
    dockers_official_GPC
    dockers_set_repository_branch
    docker_engine
    post_install
}
