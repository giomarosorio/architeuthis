#!/bin/bash
#
#===================================
set -e
#===================================
#==============UTILS================
#===================================
command_exists() {
  command -v "$@" >/dev/null 2>&1
}
#===================================
check_commands() {
  commands=("$@")
  for command_to_find in "${commands[@]}"; do
    if ! command_exists $command_to_find; then
      packages_to_install+=" $command_to_find"
    fi
  done
}
#===================================
showcommand(){
    tput setaf 2
    if [ ! "${1}" = "none" ]; then
        echo -e "${txtcommand//%1/${1}}"
        echo ""
    fi
    tput sgr0
}
#===================================
showmessage(){
    tput setaf 6
    if [ ! "${1}" = "none" ]; then
        echo -e "${txtmessage//%1/${1}}"
        echo ""
    fi
    tput sgr0
}
#===================================
showtitle(){
    tput setaf 5
    if [ ! "${1}" = "none" ]; then
    echo ""
    echo "##############################################"
    echo "${1}"
    echo "##############################################"
    echo ""
    fi
    tput sgr0
}
#===================================
check_success_command(){
    [[ $? -eq 0 ]] \
        && { return 0; } \
        || { showmessage "${1}"; exit 1; }
}
#===================================
txtcommand="> %1"
txtmessage="%1"
