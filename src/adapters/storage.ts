import { adapter, Scheme } from 'scheme-adapter';

import { StorageElement } from '../models';

const StorageElementScheme: Scheme<StorageElement> = {
  content: Buffer.from(''),
  metadata: {
    name: '',
    size: '',
    type: '',
    created: '',
    updated: '',
  },
};

export const storageAdapter = (data: any): StorageElement => adapter<StorageElement>(data, StorageElementScheme);
