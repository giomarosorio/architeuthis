jest.mock('firebase-admin', () => {
  return {
    firestore: jest.fn(() => ({})),
    database: jest.fn(() => ({})),
    auth: jest.fn(() => ({})),
    storage: jest.fn(() => ({})),
    initializeApp: jest.fn(() => ({})),
  };
});
