interface StorageMetadata {
  name: string;
  size: string;
  type: string;
  created: string;
  updated: string;
}

export interface StorageElement {
  metadata: StorageMetadata;
  content: Buffer;
}
