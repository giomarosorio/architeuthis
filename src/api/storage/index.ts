import * as express from 'express';

import { Dependencies } from '../models';
import { getElement } from './controller';
//import { deleteElement, getElement, postElement, putElement } from './controller';

const router = express.Router({ mergeParams: true });

router.get('/', (req, res) => res.send('Architeuthis API | Storage 🦑'));
router.get('/:bucket/:path', getElement(Dependencies));
//router.delete('/:bucket/:path', deleteElement(Dependencies));
//router.post('/:bucket/:path', postElement(Dependencies));
//router.put('/:bucket/:path', putElement(Dependencies));

export default router;
