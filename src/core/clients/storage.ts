import { FirebaseAdmin } from '../../repositories';
import { DeletePayload, GetPayload, Http, PostPayload, PutPayload } from '../models';

export class API implements Http {
  private client: FirebaseAdmin.config.StorageI;
  constructor(client: FirebaseAdmin.config.StorageI) {
    this.client = client;
  }
  private adapterData = <T>(data: any, adapter: (data: any) => T): T | Error => {
    if (typeof data === typeof Error) return data;
    return adapter(data);
  };
  get = async <T>({ bucketName, path }: GetPayload, adapter: (data: any) => T): Promise<T | Error> => {
    const data = await FirebaseAdmin.storage.get<T>(this.client)({ bucketName, path });
    return this.adapterData<T>(data, adapter);
  };
  post = async <T>({ bucketName, path, body }: PostPayload, adapter: (data: any) => T): Promise<T | Error> => {
    const data = await FirebaseAdmin.storage.post<T>(this.client)({ bucketName, path, body });
    return this.adapterData<T>(data, adapter);
  };
  put = async <T>({ bucketName, path, body }: PutPayload, adapter: (data: any) => T): Promise<T | Error> => {
    const data = await FirebaseAdmin.storage.put<T>(this.client)({ bucketName, path, body });
    return this.adapterData<T>(data, adapter);
  };
  delete = async ({ bucketName, path }: DeletePayload): Promise<void | Error> => {
    const data = await FirebaseAdmin.storage.remove<void>(this.client)({ bucketName, path });
    if (typeof data === typeof Error) return data;
  };
}

export const Client = new API(FirebaseAdmin.config.storage);
