import cors from 'cors';
import express from 'express';
import morgan from 'morgan';

import api from './api';

const app = express();

app.use(cors({ origin: '*' }));
app.use(morgan('combined'));
app.use(express.json());
app.use(api);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.warn(`App is running on port ${port}!`);
});
