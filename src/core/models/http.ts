export interface Path {
  bucketName: string;
  path: string;
}

export interface GetPayload extends Path {}

export interface PostPayload extends Path {
  body: any;
}

export interface PutPayload extends PostPayload {}

export interface DeletePayload extends Path {
  id?: string;
}

export interface Http {
  get: <T>(paylod: GetPayload, adapter: (data: any) => T, isNode?: boolean) => Promise<T | Error>;
  post: <T>(payload: PostPayload, adapter: (data: any) => T) => Promise<T | Error>;
  put: <T>(payload: PutPayload, adapter: (data: any) => T) => Promise<T | Error>;
  delete: (payload: DeletePayload) => Promise<void | Error>;
}
