import express from 'express';

export type Request = express.Request;
export type Response = express.Response;
export type NextFunction = express.NextFunction;

type Callback = (req: Request, res: Response, next: NextFunction) => void;

export const callbackBuilder = (callback: Callback) => (req: Request, res: Response, next: NextFunction) => callback(req, res, next);
