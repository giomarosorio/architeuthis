import * as admin from 'firebase-admin';

(() => {
  const credential = process.env.GOOGLE_APPLICATION_CREDENTIALS || '{}';
  const credentialParse = JSON.parse(credential);
  admin.initializeApp({
    credential: admin.credential.cert(credentialParse as admin.ServiceAccount),
    databaseURL: process.env.DATABASE,
    storageBucket: process.env.STORAGE,
  });
})();

export type AppI = admin.app.App;

export const firestre = admin.firestore();
export type FirestoreI = FirebaseFirestore.Firestore;

export const realtime = admin.database();
export type RealtimeI = admin.database.Database;

export const auth = admin.auth();
export type AuthI = admin.auth.Auth;

export const storage = admin.storage();
export type StorageI = admin.storage.Storage;
