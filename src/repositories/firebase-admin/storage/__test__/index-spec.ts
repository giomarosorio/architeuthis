/* eslint-disable max-lines */
import { StorageElement } from '../../../../core/models/storage';
import { errorBuilder, WARNING } from '../../../../utils/error';
import { StorageMock } from '../../../mocks';
import { makeFilesInBucket } from '../../../mocks/utils';
import { get, post, put, remove } from '..';

describe('GET | Storage methods', () => {
  const fileList: string[] = ['test.txt', 'index.ts', 'cat.jpg', 'dummy.pdf'];
  const filesInBucket = makeFilesInBucket(fileList);
  let client: StorageMock;

  it('mapping correctly from get when the Bucket does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucket);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });

    // Act
    const act = await get<StorageElement>(client)({ bucketName: 'Bucket', path: 'test.txt' });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from get when the Bucket exists and File does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucket);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });

    // Act
    const act = await get<StorageElement>(client)({ bucketName: '', path: 'doesnotexist.txt' });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from get when the Bucket exists and File exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucket);
    const expected = filesInBucket['cat.jpg'];

    // Act
    const act = await get<StorageElement>(client)({ bucketName: '', path: 'cat.jpg' });

    // Assert
    expect(act).toEqual(expected);
  });
});

describe('POST | Storage methods', () => {
  let fileList: string[] = ['test.txt', 'index.ts', 'dummy.pdf'];
  let fileListComplete: string[] = ['test.txt', 'index.ts', 'cat.jpg', 'dummy.pdf'];
  const filesInBucket = makeFilesInBucket(fileList);
  const filesInBucketComplete = makeFilesInBucket(fileListComplete);
  let client: StorageMock;

  it('mapping correctly from post when the Bucket does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucketComplete);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });

    // Act
    const fileName = 'cat.jpg';
    const fileData = filesInBucketComplete[fileName];
    const act = await post<StorageElement>(client)({ bucketName: 'Bucket', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from post when the Bucket exists and File exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucketComplete);
    const expected = errorBuilder({
      type: WARNING,
      code: 405,
      message: 'Action not allowed. It is only possible to execute GET, PUT and DELETE on the file.',
    });

    // Act
    const fileName = 'cat.jpg';
    const fileData = filesInBucketComplete[fileName];
    const act = await post<StorageElement>(client)({ bucketName: '', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from post when the Bucket exists and File does not exists', async () => {
    // Arrange
    const fileName = 'cat.jpg';
    client = new StorageMock(filesInBucket);
    const expected = filesInBucketComplete[fileName];

    // Act
    const fileData = filesInBucketComplete[fileName];
    const act = await post<StorageElement>(client)({ bucketName: '', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });
});

describe('PUT | Storage methods', () => {
  let fileList: string[] = ['cat.jpg', 'index.ts', 'dummy.pdf'];
  let fileListComplete: string[] = ['test.txt', 'index.ts', 'cat.jpg', 'dummy.pdf'];
  const filesInBucket = makeFilesInBucket(fileList);
  const filesInBucketComplete = makeFilesInBucket(fileListComplete);
  let client: StorageMock;

  it('mapping correctly from put when the Bucket does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucketComplete);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });

    // Act
    const fileName = 'test.txt';
    const fileData = filesInBucketComplete[fileName];
    const act = await put<StorageElement>(client)({ bucketName: 'Bucket', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from put when the Bucket exists and File does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucket);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });

    // Act
    const fileName = 'test.txt';
    const fileData = 'PUT | test';
    const act = await put<StorageElement>(client)({ bucketName: '', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });
  it('mapping correctly from put when the Bucket exists and File exists', async () => {
    // Arrange
    const fileName = 'test.txt';
    let date: Date = new Date();
    const newElement: StorageElement = {
      metadata: {
        name: fileName,
        size: `${Math.floor(Math.random() * 1000000000) + 1} bytes`,
        type: fileName.split('.')[1],
        created: date.toString(),
        updated: date.toString(),
      },
      content: Buffer.from('This a new file test.', 'utf8'),
    };
    client = new StorageMock(filesInBucketComplete);
    const expected = newElement;

    // Act
    const fileData = newElement;
    const act = await put<StorageElement>(client)({ bucketName: '', path: fileName, body: fileData });

    // Assert
    expect(act).toEqual(expected);
  });
});

describe('REMOVE | Storage methods', () => {
  let fileList: string[] = ['test.txt', 'cat.jpg', 'index.ts'];
  let fileListComplete: string[] = ['test.txt', 'index.ts', 'cat.jpg', 'dummy.pdf'];
  const filesInBucket = makeFilesInBucket(fileList);
  const filesInBucketComplete = makeFilesInBucket(fileListComplete);
  let client: StorageMock;

  it('mapping correctly from remove when the Bucket does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucketComplete);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'Bucket not found.' });

    // Act
    const act = await remove<StorageElement>(client)({ bucketName: 'Bucket', path: 'testRemove.txt' });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from remove when the Bucket exists and File does not exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucket);
    const expected = errorBuilder({ type: WARNING, code: 404, message: 'File not found.' });

    // Act
    const fileName = 'dummy.pdf';
    const act = await remove<StorageElement>(client)({ bucketName: '', path: fileName });

    // Assert
    expect(act).toEqual(expected);
  });

  it('mapping correctly from remove when the Bucket exists and File exists', async () => {
    // Arrange
    client = new StorageMock(filesInBucketComplete);

    // Act
    const fileName = 'dummy.pdf';
    const act = await remove<void>(client)({ bucketName: '', path: fileName });

    // Assert
    expect(act).toBeUndefined();
  });
});
