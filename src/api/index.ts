import * as express from 'express';

import storage from './storage';

const router = express.Router();

router.get('/', (req, res) => res.send('Architeuthis API 🦑'));

router.use('/storage', storage);

export default router;
